import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:webviw_fl01/models/all_good_wrap_model.dart';
import 'package:webviw_fl01/screens/favorite_goods/favorite_goods.dart';
import 'package:webviw_fl01/screens/main_customer/widgets/search_field_widget.dart';
import 'package:webviw_fl01/screens/main_customer/widgets/top_category_bloc_widget.dart';

class MainCustomer extends StatefulWidget {
  const MainCustomer({Key? key}) : super(key: key);

  @override
  _MainCustomerState createState() => _MainCustomerState();
}

class _MainCustomerState extends State<MainCustomer> {
  Dio dio = Dio();
  var data;
  List titles = [
    'TOM TAILOR/Джемпер',
    'VIZANI / Куртка',
    'TOM TAILOR/Джемпер',
    'VIZANI / Куртка'
  ];
  List images = [
    'images/clothes01.png',
    'images/clothes02.png',
    'images/clothes01.png',
    'images/clothes02.png'
  ];


  @override
  void initState() {
    getGoods();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: data != null ?  Column(children: [
          SizedBox(
            height: 15,
          ),
          SearchFieldWidget(),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Row(
              children: [
                TopCategoryWidget(
                  title: 'Одежда',
                  urlImg: 'images/customer_main/clothes.png',
                ),
                SizedBox(
                  width: 38,
                ),
                TopCategoryWidget(
                  title: 'Еда',
                  urlImg: 'images/eat.png',
                ),
                SizedBox(
                  width: 25,
                ),
                TopCategoryWidget(
                  title: 'Автотовары',
                  urlImg: 'images/avtogoods.png',
                ),
                SizedBox(
                  width: 30,
                ),
                TopCategoryWidget(
                  title: 'Аптека',
                  urlImg: 'images/pharmy.png',
                )
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(left: 16, right: 16),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            child: Image.asset(
              'images/banner01.png',
              width: double.infinity,
              height: 120,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Flexible(
            child: GridView.builder(
              shrinkWrap: true,
              itemCount: titles.length,
              itemBuilder: (BuildContext context, int index) {
                return  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: Container(
                    padding: EdgeInsets.only(top: 16, left: 13, right: 13),
                    decoration: BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        borderRadius: BorderRadius.circular(5)
                    ),
                    child: Column(

                      children: [
                         Flexible(
                           child: ClipRRect(

                              borderRadius: BorderRadius.circular(5),
                              child: Image.network(data['data'][index]['images'][0]['medium'],
                              width: 133, height: 180,),
                            ),
                         ),

                        SizedBox(height: 8,),
                        Center(child: Text(data['data'][index]['title'])),
                        SizedBox(height: 8,),
                        Center(
                          child: Row(
                            children: [
                              Text(data['data'][index]['price']),
                              SizedBox(width: 10,),
                              Text('17 900',
                                style: TextStyle(
                                  decoration: TextDecoration.lineThrough
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
              ),
            ),
          ),
        ]): Center(
          child: CupertinoActivityIndicator(),
        )
      ),
      backgroundColor: Color(0xFFF6F6FC),
    );
  }

  getGoods() async {
    Response response = await dio.get('http://91.201.214.194/api/v1/products');
    print(response.data['data'][0]['title']);
    print(response.data['data'][0]['description']);
    setState(() {
      data = response.data;
    });
    print(data['data'][0]['price']);

  }
}

