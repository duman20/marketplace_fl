
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopCategoryWidget extends StatelessWidget {
  final String title;
  final String urlImg;
  const TopCategoryWidget({
    Key? key,
    required this.title,
    required this.urlImg
  }
  ) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Expanded(
      child: Column(
        children: [
          Image.asset(urlImg),
          SizedBox(height: 8,),
          Text(title,
            style: TextStyle(fontSize: 12, height: 1.2, color: Color(0xFF0C1A30),),
          )
        ],
      ),
    );
  }
}
