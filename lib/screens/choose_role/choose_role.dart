import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webviw_fl01/screens/choose_role/widgets/choose_role_type_widget.dart';
import 'package:webviw_fl01/screens/main_customer/main_customer.dart';
import 'package:webviw_fl01/screens/main_seller/main_seller.dart';

import '../customer_menu.dart';

class ChooseRole extends StatelessWidget {
  const ChooseRole({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Color(0xFFF6F6FC),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16, top: 25),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Выберите \nвариант',
                    style: TextStyle(color: Color(0xFF2B2D33),
                    fontSize: 32,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Rubik',
                    )
                  ),
                ),
              ),
              SizedBox(height: 20,),
             ChooseRoletypeWidget(
                 title: 'Покупатель',
                 description: 'Присоединяйтесь и вперед за покупками',
                 urlImg: 'images/customer_first.png',
                 onPressed: () {
                   print('123334');
                   Navigator.of(context).push(
                       MaterialPageRoute(
                           builder: (context) => CusomerMenu()
                       )
                   );
                 }
                 ),
              SizedBox(height: 15,),
             ChooseRoletypeWidget(
                 title: 'Продавец',
                 description: 'Начните зарабатывать с нами',
                 urlImg: 'images/seller_first.png',
               onPressed: () {
                         Navigator.of(context)
                             .push(MaterialPageRoute(
                             builder: (context)=> MainSeller()
                           )
                         );
                       },
             )
            ],
          ),
        ),
      ),
    );
  }
}