
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChooseRoletypeWidget extends StatelessWidget {
  final String title;
  final String description;
  final String urlImg;
  final VoidCallback? onPressed;
  const ChooseRoletypeWidget({
    Key? key,
    required this.title,
    required this.description,
    required this.urlImg,
    required this.onPressed,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        highlightColor: Colors.orange.withOpacity(0.3),
        splashColor: Colors.red.withOpacity(0.5),
        //padding: EdgeInsets.all(0),
        onTap: onPressed, //print('sgsghdh');
        //onDoubleTap: () { onPressed; },
        child: Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Ink(
            decoration: BoxDecoration(
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.all(Radius.circular(10.0))
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 25, right: 15),
              child: Row(
                children: [
                  Expanded(
                    child: Center(
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(title,
                              style: TextStyle(
                                  fontSize: 16,
                                  height: 1.2,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFF2B2D33)
                              ),
                            ),
                          ),
                          SizedBox(height: 5,),
                          Text(description,
                            style: TextStyle(
                                fontSize: 14,
                                height: 1.2,
                                color: Color(0xFF2B2D33)
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: 15,),
                  Expanded(
                      child: Image.asset(urlImg)
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}