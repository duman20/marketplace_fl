import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'favorite_goods/favorite_goods.dart';
import 'main_customer/main_customer.dart';

class CusomerMenu extends StatefulWidget {
  const CusomerMenu({Key? key}) : super(key: key);

  @override
  _CusomerMenuState createState() => _CusomerMenuState();
}

class _CusomerMenuState extends State<CusomerMenu> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: SafeArea(
          child:    CupertinoTabScaffold(
            tabBar: CupertinoTabBar(
              backgroundColor: Color(0xFFFFFFFF),
              items: [
                BottomNavigationBarItem(
                    icon: SvgPicture.asset('images/home_icon.svg'),
                    activeIcon: SvgPicture.asset('images/home_icon_active.svg')
                ),
                BottomNavigationBarItem(
                    icon: SvgPicture.asset('images/love_icon.svg'),
                    activeIcon: SvgPicture.asset('images/love_icon_active.svg')
                ),

              ],
            ), tabBuilder: (
              BuildContext context, int index
              )  {
            return CupertinoTabView(builder: (context) {

              switch (index) {
                case 0:
                  return MainCustomer();
                case 1:
                  return FavoriteGoods();
                default:
                  return MainCustomer();
              }
            }
            );
          },
          ),
        )
    );
  }
}

    
    