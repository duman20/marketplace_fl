
import 'package:json_annotation/json_annotation.dart';

part 'all_goods_model.g.dart';

@JsonSerializable()
class AllGoodsModel {
  final int? id;
  final String? title;
  final String? description;
  final String? original_price;
  final String? discount_percent;
  final String? price;
  final String? images;


  AllGoodsModel({
    required this.id,
    required this.title,
    required this.description,
    required this.original_price,
    required this.discount_percent,
    required this.price,
    required this.images,
    });

  factory AllGoodsModel.fromJson(Map<String, dynamic> json) =>_$AllGoodsModelFromJson(json);
  Map<String, dynamic> toJson() => _$AllGoodsModelToJson(this);

}