
import 'package:json_annotation/json_annotation.dart';

import 'all_goods_model.dart';
part 'all_good_wrap_model.g.dart';

@JsonSerializable()
class AllGodWrap{
  final List<AllGoodsModel>? goods;

  AllGodWrap({
    required this.goods
  });
}