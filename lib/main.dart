
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webviw_fl01/screens/choose_role/choose_role.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: AnimatedSplashScreen(
          backgroundColor: Color(0xFF3568FF),
          duration: 1000,
          splashTransition: SplashTransition.scaleTransition,
          nextScreen: ChooseRole(),
          splash: Image.asset('images/splash.png'),
        )
      )
    );
  }
}
